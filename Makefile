.PHONY: run

# certs and output
OUTPUT_FILE=WB-V2.0.0.apk
# CHECK IF APP IS NOT IN SIMULATOR MODE!!!!!!!!!!
ALIAS=white-book
KEYPASS=white-book

# Example: /Users/your_user/Dev/release_keystore.keystore
KEYSTORE=WhiteBook.keystore
ANDROID_PATH=/usr/local/Cellar/android-sdk/24.2

# UNSIGNED=platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk
UNSIGNED=platforms/android/build/outputs/apk/android-release-unsigned.apk

# Replace with your package name defined in config.xml
PACKAGE='eu.innovapp.app.whitebook'

# Remove and readd node_modules iOS and Android Platforms
full-update: rm-add-node rm-add-android rm-add-ios

# Remove and readd node_modules
rm-add-node:
	rm -rf node_modules
	npm i

# Update Android Platform
rm-add-android:
	ionic cordova platform rm android
	rm -rf platforms/android
	ionic cordova platform add android@6.3.0

# Create Android Signed APK
release-android: build-android sign-android allign-android

# Build Android Unsigned APK
build-android:
	rm -f ./build/${OUTPUT_FILE}
	ionic cordova build android --release

# Sign already created unsigned APK
sign-android:
	jarsigner -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore ${KEYSTORE} -storepass ${KEYPASS} ${UNSIGNED} ${ALIAS}

# Zip allign signed APK
allign-android:
	${ANDROID_PATH}/build-tools/23.0.2/zipalign -v 4 ${UNSIGNED} ./build/${OUTPUT_FILE}

# Run on android with --livereload
run-android:
	ionic cordova run android --livereload

# Install signed APK on a device
install-android-device:
	adb install -r ./build/${OUTPUT_FILE}

# Run App already installed on a device
execute-android:
	adb shell am start -n ${PACKAGE}/${PACKAGE}.MainActivity

# Monitor logs of launched app on device
log-android:
	adb logcat | grep `adb shell ps | grep ${PACKAGE} | cut -c10-15`

# Build & Sign & Allign & Install & Run & Monitor App on device
run-android: sign-android install-android-device execute-android log-android

# update ios platform
rm-add-ios:
	ionic cordova platform rm ios
	rm -rf platforms/ios
	ionic cordova platform add ios

# create ios build
release-ios:
	ionic cordova build ios --prod

# run ios
run-ios:
	ionic cordova run ios --target "iPhone-6, 11.2" --livereload
