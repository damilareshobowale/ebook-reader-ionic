declare var ePub: any;
export class Book {
  id: string;
  cover: string;
  label: string;
  file: string;
  metadata?: object;
}
export class LastLocation {
  bookid: string;
  location: string;
}


export class BookMark {
  id?: string;
  bookid: string;
  excerpt: string;
  location: string;
  position: string;
}
export class Highlight {
  id?: string;
  bookid: string;
  text: string;
  location: string;
  position: string;
  cfiRange: string;
}
export class BooksService {

  private offlineBooks: Book[] = [
    {
      id: "1",
      cover: "../assets/imgs/cover.png",

      label: "Moby Dick (unpacked)",
      file: "../assets/books/moby-dick/"
    },
    {
      id: "2",
      cover: "../assets/imgs/cover.png",
      label: "Moby Dick (.epub)",
      file: "../assets/books/moby-dick.epub"
    },
    {
      id: "3",
      cover: "../assets/imgs/cover.png",
      label: "Open (unpacked)",
      file: "../assets/books/open/"
    },
    {
      id: "4",
      cover: "../assets/imgs/cover.png",
      label: "Remote with correct headers [works everywhere] (.epub)",
      file: "https://yatsa.betamo.de/ionic-epubjs/Metamorphosis-jackson.epub"
    },
    {
      id: "5",
      cover: "../assets/imgs/cover.png",
      label: "Mobi Dick Remote (.epub)",
      file: "https://s3.amazonaws.com/moby-dick/moby-dick.epub"
    }
  ];

  public getAll(): Promise<Book[]> {
    const promises = this.offlineBooks.map(book => {
      return book;
    });
    return Promise.all(promises);
  }
}
