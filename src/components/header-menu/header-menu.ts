import { Component } from '@angular/core';
import { Nav, MenuController, NavController, LoadingController, AlertController } from 'ionic-angular';

import { AuthService } from '../../pages/auth/auth.service';
import { AuthPage } from '../../pages/auth/auth';

/**
 * Generated class for the HeaderMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'header-menu',
  templateUrl: 'header-menu.html',
  providers: [AuthService],
})
export class HeaderMenuComponent {
  loading: any;

  constructor(public authService: AuthService,
              public menuCtrl: MenuController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public navCtrl: NavController
              ) {
    this.menuCtrl.enable(true);
  }

  onLogout()
  {
    // this.loading = this.loadingCtrl
        // .create({ spinner: 'crescent', content: 'Logging out...'});
    // this.loading.present();
    // this.authService.logout();
    this.showSuccessAlert('Logout Success', 'You have successfully logout', AuthPage);
    // this.loading.dismiss();
  }

  private showSuccessAlert(title: string, message: string, linkpage: any) {
    let alert = this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'OK',
            handler: () => {
            this.navCtrl.setRoot(linkpage); 
        }
          },

        ]
      });
      alert.present();
  }

}
