import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';

import { Book } from '../../providers/books-service';
import { BookPage } from '../book/book';

/**
 * Generated class for the SavedItemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 declare var ePub: any;

@IonicPage()
@Component({
  selector: 'page-saved-items',
  templateUrl: 'saved-items.html',
})
export class SavedItemsPage implements OnInit {
  isEmpty = true;
  isLoading = false;
  books: Book[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private documentViewer: DocumentViewer,
              private fileItem: File,
              private fileTransfer: FileTransfer,
              private platform: Platform,
              private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SavedItemsPage');
  }

  ngOnInit() {
    this.openSavedBooks()
  }

  openLocalPDF() {
    const options: DocumentViewerOptions = {
      title: 'PDF'
    }
    
    this.documentViewer.viewDocument('assets/books/doc1.pdf', 'application/pdf', options);
  }

  openSavedBooks() {
    this.isLoading = true;
    let path = null;

    if (this.platform.is('ios')) {
        path = this.fileItem.documentsDirectory;
    } else {
      path = this.fileItem.dataDirectory;
    }

    this.fileItem.checkDir(path, 'legalebooks').then(data => {
      
    }).catch(error => {
      this.isEmpty = true;
      this.showErrorAlert('Directory not exists', 'Sorry this directory do not exists');

    });
    this.isLoading = false;
  }

  show(book) {
    console.log('show', book);
    this.navCtrl.setRoot(BookPage, {
      book: book
    });
  }

  async showErrorAlert(title: string, message: string) {
    const alert = await this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'Okay',
            role: 'cancel',
          },
        ]
      });
      await alert.present();
  }

}
