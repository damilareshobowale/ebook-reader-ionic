import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavedItemsPage } from './saved-items';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(SavedItemsPage),
  ],
})
export class SavedItemsPageModule {}
