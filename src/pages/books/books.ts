import { Component } from '@angular/core';
import { IonicPage, MenuController, AlertController, LoadingController, NavController, NavParams, ModalController, Platform, Events } from 'ionic-angular';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { File } from '@ionic-native/file';
import { User } from '../auth/user.model';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Subscription } from 'rxjs';

import { BookPage } from '../book/book';
import { Book, BooksService } from '../../providers/books-service';
import { ParseBook } from '../../providers/parse-db-book';
import { DbService } from '../../providers/db-service';
import { PaymentHistoryService } from '../payment-history/payment-history.service';

declare var ePub: any;

@Component({
  selector: 'page-books',
  templateUrl: 'books.html',
  providers: [PaymentHistoryService],
})

export class BooksPage {
  userId: string;
  loading: any;
  onlineBooks: any;
  books: Book[];
  paymentsSub: Subscription;

  constructor(public events: Events,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    private modal: ModalController,
    public dbService: DbService,
    private lCtrl: LoadingController,
    private fileItem: File,
    private fileTransfer: FileTransfer,
    private alertCtrl: AlertController,
    private documentViewer: DocumentViewer,
    private menuCtrl: MenuController,
    private paymentHistoryService: PaymentHistoryService) {
    menuCtrl.enable(true);
    this.userId = navParams.get('userId');
    events.subscribe("booksRetrieved", (books) => {
      this.books = books;
    })

    this.platform.ready().then((readySource) => {
      this.dbService.getAllBooks().then((books) => {
        this.books = books;
      });
    });

    console.log(this.books);

  }

  onBook(book) {
    this.navCtrl.setRoot(BookPage, {}, { animate: true, direction: "forward" });
  }

  openModal(book) {
    const myModal = this.modal.create('AboutModalPage', { book: book });
    myModal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BooksPage');
  }

  show(book) {
    console.log('show', book);
    this.navCtrl.setRoot(BookPage, {
      book: book
    });
  }

  saveOffline(book) {
    this.downloadocalPDF(book.file)
    
    
  }

  downloadocalPDF(bookPath) {
    this.loading = this.lCtrl
        .create({ spinner: 'crescent', content: 'Downloading...'});
    this.loading.present();
    let path = null;

      if (this.platform.is('ios')) {
          path = this.fileItem.documentsDirectory;
      } else {
        path = this.fileItem.dataDirectory;
      }


      const transfer = this.fileTransfer.create();
      const rndom = Math.random().toString();
      transfer.download(bookPath, path + 'legalebooks/' + rndom+ '.ebup')
      .then(entry => {
        let url = entry.toURL();
        this.loading.dismiss();
        this.showSuccessAlert('Download Success', 'Book downloaded successfully, access your books on your device storage ' + path);
        // this.documentViewer.viewDocument(url, 'application/pdf', {});
      }, (error) => {
      // handle error
      this.showErrorAlert('Downlaod Failed', 'Sorry the book is unable to download. Please check your network and try again.');
      
    })
  }

    async showErrorAlert(title: string, message: string) {
    const alert = await this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'Okay',
            role: 'cancel',
          },
        ]
      });
      await alert.present();
  }


  async showSuccessAlert(title: string, message: string) {
    let alert = await this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
          },

        ]
      });
      await alert.present();
  }
}
