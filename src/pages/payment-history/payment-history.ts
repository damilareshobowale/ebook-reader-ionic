import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MenuController, LoadingController, AlertController, NavController, IonicPage, NavParams } from 'ionic-angular'
import { Subscription } from 'rxjs';


import { PaymentHistoryService } from './payment-history.service';
import { SubscriptionPage } from '../subscription/subscription';
import { Payment } from './payment.model'

/**
 * Generated class for the PaymentHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-history',
  templateUrl: 'payment-history.html',
  providers: [PaymentHistoryService],
})
export class PaymentHistoryPage {
  userId: string;
  amount: number;
  created_on: Date;
  email: string;
  status: string;
  expiry: Date;
  type: string;
  ref_id: string;

  payment:  Payment;
  isLoading = false;
  isEmpty = false;
  private paymentsSub: Subscription;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  private paymentHistoryService: PaymentHistoryService, 
  private menu: MenuController,
  private alertCtrl:  AlertController) {
    this.userId = navParams.get('userId');
    this.email = navParams.get('email');
  }

  ngOnInit() {
    this.isLoading = true;
    this.isEmpty = true;
    this.paymentsSub = this.paymentHistoryService.fetchPayments(this.userId).subscribe(payment =>
    {
      this.isLoading = false;
      this.isEmpty = false;
      this.payment = payment;
      this.amount = this.payment.amount;
      this.created_on = new Date(this.payment.created_on);
      this.email = this.payment.email;
      this.expiry = new Date(this.payment.expiry);
      this.ref_id = this.payment.ref_id;
      this.status = this.payment.status;
      this.userId = this.payment.user_id;
      this.type = this.payment.type;

    }, 
    error =>  {
        this.showErrorAlert('No Payment Yet', 'No payment has been done so far');
        return;
      });
    
    this.isLoading = false;
  }

  ionViewWillEnter() {
  }

  onNavigationToSubscription()
  {
    this.navCtrl.push(SubscriptionPage, {
      userId: this.userId, 
      email: this.email,
    });
  }

  ngOnDestroy() {
    if (this.paymentsSub) {
      this.paymentsSub.unsubscribe();
    }
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  async showErrorAlert(title: string, message: string) {
    let alert = await this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'Okay',
            role: 'cancel',
          },
        ]
      });
      await alert.present();
  }



}
