export class Payment {

    constructor(
        public id: string, 
        public amount: number, 
        public created_on: Date, 
        public email: string, 
        public expiry: Date, 
        public ref_id: string, 
        public status: string, 
        public user_id: string, 
        public type: string,
    ) {}
}