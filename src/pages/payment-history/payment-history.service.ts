import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { tap, map, take, switchMap } from 'rxjs/operators';

import { AuthService } from '../auth/auth.service';
import { Payment } from './payment.model'
import { environment } from '../../environments/environment';

interface PaymentHistoryData {
    id: string;
    amount: number;
    created_on: Date;
    email: string;
    expiry: Date;
    ref_id: string;
    status: string;
    user_id: string;
    type: string;
}

@Injectable()

export class PaymentHistoryService { 
    private _payments = new BehaviorSubject<Payment[]>([]);

    get payments() {
        return this._payments.asObservable();
    }

    constructor(private http: HttpClient) {}

    fetchPayments(id: string) {
        return this.http.get<PaymentHistoryData>(`${environment.firebaseDatabaseURL}/payments/${id}.json`)
        .pipe(
            map(
            resData => {
                return new Payment (
                        id,
                        resData.amount, 
                        new Date(resData.created_on), 
                        resData.email, 
                        new Date(resData.expiry),
                        resData.ref_id,
                        resData.status,
                        resData.user_id,
                        resData.type,
                )
            }
        ));
    }

    getPayments (id: string) {
        return this.http.get<PaymentHistoryData>(`${environment.firebaseDatabaseURL}/payments/${id}.json`).pipe(tap(
            resData => {
                // Do some action on successful
                return new Payment(
                    id,
                    resData.amount,
                    new Date(resData.created_on), 
                    resData.email, 
                    new Date(resData.expiry),
                    resData.ref_id,
                    resData.status,
                    resData.user_id,
                    resData.type,
                )
                console.log("This is success Data" + resData);
            },
            errData => {
                // Do some action on failure
                console.log("This is error Data" + errData);
            }
        )).subscribe();
    }
}