import { NgModule } from '@angular/core';
import { Angular4PaystackModule } from 'angular4-paystack';
import { IonicPageModule } from 'ionic-angular';
import { PaystackPage } from './paystack';


@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(PaystackPage),
  ],
})
export class PaystackPageModule {}
