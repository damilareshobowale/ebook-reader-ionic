import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take, map, tap, delay, switchMap } from 'rxjs/operators';

import { AuthService } from '../auth/auth.service';
import { environment } from '../../environments/environment';
import { Payment } from '../payment-history/payment.model';

interface PaystackData {
    id: string;
    amount: number;
    created_on: Date;
    email: string;
    expiry: Date;
    ref_id: string;
    status: string;
    user_id: string;
    type: string;
}

@Injectable()

export class PaystackService { 

    constructor(private http: HttpClient) {}

    store (id: string, email: string, ref_id: string, amount: number, created_on: Date, expiry: Date, type: string) {
        return this.http.patch(`${environment.firebaseDatabaseURL}/payments/${id}.json`, {
            id: null,
            user_id: id,
            email: email, 
            ref_id: ref_id,
            amount: amount,
            created_on: created_on,
            expiry: expiry,
            status: 'active',
            type: type,
        }).pipe(tap(
            resData => {
                // Do some action on successful
                console.log("This is success Data" + resData);
            },
            errData => {
                // Do some action on failure
                console.log("This is error Data" + errData);
            }
        )).subscribe();
    }

    getPayment(id:string) {
        return this.http.get<PaystackData>(`${environment.firebaseDatabaseURL}/payments/${id}.json`)
        .pipe(map (resData => {
                    console.log('This is my own' + resData)
                    if (resData == null) {
                        return null;
                    }
                    return new Payment (
                        id,
                        resData.amount,
                        new Date(resData.created_on),
                        resData.email,
                        new Date(resData.expiry),
                        resData.ref_id,
                        resData.status,
                        resData.user_id,
                        resData.type,
                    )
                }));
    }
}