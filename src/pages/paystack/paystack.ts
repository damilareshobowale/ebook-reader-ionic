import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, ViewController,  Platform,  NavParams  } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { BehaviorSubject, from, Subscription } from 'rxjs';

import { InAppBrowser } from 'ionic-native';
import { DashboardPage } from '../dashboard/dashboard';
import { environment } from '../../environments/environment';

import { PaystackService } from './paystack.service';
import { Payment } from '../payment-history/payment.model';

/**
 * Generated class for the PaystackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-paystack',
  templateUrl: 'paystack.html',
  providers: [PaystackService],
})
export class PaystackPage {
  userId: string;

  inputData: any;
  
  price: number;

  email: string;

  key: string;

  currency: string;

  ref: number;

  plan: string;

  created_on: Date;

  expiry: Date;

  type: string;

  payment: Payment;

  private paymentSub: Subscription;

  constructor(
    public navCtrl: NavController, 
    public alertCtrl: AlertController, 
    public navParams: NavParams,   
    public loading: LoadingController,  
    public platform: Platform, 
    public viewCtrl: ViewController,
    private paystackService: PaystackService,
    ) {
      this.userId = navParams.get('userId');
    }

  ngOnInit() {
    this.inputData = this.navParams.get('inputData');
    let chargeAmount = this.inputData.price*100;
    this.price = chargeAmount;
    this.email = this.inputData.email;
    // this.email = 'mail@gmail.com';
    this.key = environment.paystackPublicAPI;
    this.currency = this.inputData.currency;
    this.ref = Math.floor(Math.random()*(9999999999-1)+1);
    this.type = this.inputData.type;
    // this.type = 'monthly';

  
  }

  ionViewDidLoad() {
    this.userId = this.navParams.get('userId');
    console.log(this.userId);
  }

  paymentCancel() {
    this.showErrorAlert('Payment Canceled', 'You have cancel the payment.');
  }

  paymentDone(event: any) {
    this.created_on = new Date();
    console.log(this.userId);
    this.expiry = this.returnExpiry(this.type);
    this.userId = this.navParams.get('userId');
    //check whether there is subscription currently available
    this.paymentSub = this.paystackService.getPayment(this.userId).subscribe(payment =>
    {
      this.payment = payment;
      if (payment == null) {
        this.paystackService.store(this.userId, this.email, event.reference, this.price/100, this.created_on, this.expiry, this.type); // pass in user id instead of the 2
        this.showSuccessAlert('Payment Successful', 'You have successfully subscribed to ' + this.type + '  Plan. Payment Invoice has been sent to your email. Reference number is ' + event.reference, DashboardPage);
        return;
      }

      this.price = this.price + this.payment.amount;
      if (new Date(this.payment.expiry) < new Date()) { // it has expired
          this.expiry = new Date(this.expiry.getTime());
      }
      else { // the expiring is still available
        this.expiry = new Date(this.expiry.getTime() + this.payment.expiry.getTime());
      }
       this.paystackService.store(this.userId, this.email, event.reference, this.price/100, this.created_on, this.expiry, this.type); // pass in user id instead of the 2
      this.showSuccessAlert('Payment Successful', 'You have successfully renewed your plan ' + this.type + '. Payment Invoice has been sent to your email. Reference number is ' + event.reference, DashboardPage);
      return;
      
      console.log('This is get payment ' + this.payment.amount + this.payment.amount);
    }, 
    error =>  {
         this.paystackService.store(this.userId, this.email, event.reference, this.price/100, this.created_on, this.expiry, this.type); // pass in user id instead of the 2
        this.showSuccessAlert('Payment Successful', 'You have successfully subscribed to ' + this.type + '  Plan. Payment Invoice has been sent to your email. Reference number is ' + event.reference, DashboardPage);
    });
   
  }

  whatsPaystack() {
    this.platform.ready().then(() => {
      let browser = new InAppBrowser("https://paystack.com/what-is-paystack", "_system");
    });  
  }

  private showSuccessAlert(title: string, message: string, linkpage: any) {
    let alert = this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'OK',
            handler: () => {
            this.navCtrl.setRoot(linkpage, {
              userId: this.userId
            }); 
        }
          },

        ]
      });
      alert.present();
  }

  private showErrorAlert(title: string, message: string) {
    let alert = this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'Okay',
            role: 'cancel',
          },
        ]
      });
      alert.present();
  }

  private returnExpiry(type) {
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth(); 
    let day = date.getDate();
    if (type == 'Monthly') {
      return new Date(year, month + 1, day);
    }
    else return new Date(year + 1, month, day);
  }

}
