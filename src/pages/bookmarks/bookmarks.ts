import { Component } from '@angular/core';
import { NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { BookMark } from '../../providers/books-service';

@Component({
  selector: 'page-bookmarks',
  templateUrl: 'bookmarks.html'
})
export class Bookmarks {

  bookmarks: BookMark[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private dbService: DbService, public events: Events, private alertController: AlertController) {
    this.bookmarks = navParams.data;
  }

  select(bookmark: BookMark) {
    debugger
    const confirm = this.alertController.create({
      title: 'Bookamrk',
      message: bookmark.excerpt,
      buttons: [
        {
          text: "Close",
          role: "cancel",
          handler: () => {
            console.log('Cancel clicked');
          },
        },
        {
          text: "Locate",
          handler: data => {
            this.events.publish('select:location', bookmark.location);
            this.events.publish('booksDetailsModalShow', false);
          }
        }

      ]
    });
    confirm.present();
  }


  delete(bookmark) {

    const confirm = this.alertController.create({
      title: 'Confirm',
      message: " A you sure to delete this bookmark",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log('Cancel clicked');
          },
        },
        {
          text: "Delete",
          handler: data => {
            this.events.publish('delete:bookmark',bookmark)
          }
        }
      ]
    });
    confirm.present();
  }


}
