import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaystackPage } from '../paystack/paystack';

/**
 * Generated class for the SubscriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscription',
  templateUrl: 'subscription.html',
})
export class SubscriptionPage {
  inputData: any;
  userId: string;
  email: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.userId = navParams.get('userId');
     this.email = navParams.get('email');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionPage');
  }

  checkOut(title: string, price: number, currency: string, type: string) 
  {
    let inputData = {
      title: title,
      price: price,
      email: this.email,
      currency: currency,
      type: type
    }
    
    this.navCtrl.push(PaystackPage, {inputData, userId: this.userId});
  }

}
