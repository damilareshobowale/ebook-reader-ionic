import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { Plugins } from '@capacitor/core';
import { BehaviorSubject, from, Subscription } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';

import { BooksPage } from '../books/books';
import { PaystackPage } from '../paystack/paystack';
import { SubscriptionPage } from '../subscription/subscription';
import { PaymentHistoryPage } from '../payment-history/payment-history';
import { SavedItemsPage } from '../saved-items/saved-items';
import { ProfilePage } from '../profile/profile';

import { AuthService } from '../auth/auth.service';
import { ProfileService } from '../profile/profile.service';
import { PaymentHistoryService } from '../payment-history/payment-history.service';
import { AuthPage } from '../auth/auth';
import { Profile } from '../profile/profile.model';
import { Payment } from '../payment-history/payment.model';
import { environment } from '../../environments/environment';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
  providers: [AuthService, ProfileService, PaymentHistoryService]
})

export class DashboardPage implements OnInit {
  loading: any;
  email: string;
  fullName: string;
  userId: any;
  firstName: string;
  lastName: string;
  profile: Profile;
  payment: Payment;
  isAllowed = false;
  isLoading = false;
  private profileSub: Subscription;
  private paymentSub: Subscription;

  constructor(
    public navCtrl: NavController, 
    private menuCtrl: MenuController,
    public navParams: NavParams,
    public authService: AuthService,
    public paymentHistoryService: PaymentHistoryService,
    public profileService: ProfileService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
      this.menuCtrl.enable(true);
      this.userId = navParams.get('userId');
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad DashboardPage');
  //   console.log(this.userId);
  //   this.profileSub = this.profileService.getProfile(navParams.get('userId')).subscribe(profile => {
  //     this.profile  = profile;
  //   });

  // }

  ionViewWillEnter() {

  }

  onNavigateToBooksPage() {
    this.userId = this.navParams.get('userId');
    this.paymentSub = this.paymentHistoryService.fetchPayments(this.navParams.get('userId'))
        .subscribe(
          payment => {
            this.payment = payment;
            this.navCtrl.push(BooksPage, {
              userId: this.userId,
              emai: this.email,
            });
          },
          error =>  {
            this.showErrorAlert('No Subscription', 'You have no subscription. Please kindly pay to subscribe.');
            return
          }
        )
  }

  onNavigateToSubscriptionPage() {
    this.navCtrl.push(SubscriptionPage, {
      userId: this.userId,
      email: this.email
      });
  }

  ngOnInit() {
    if (this.userId == null) {
      this.showErrorAlert('Please login', 'To continue, please login to your account');
      this.navCtrl.setRoot(AuthPage);
      return;
    }
    this.userId = this.navParams.get('userId');
    this.isLoading = true;
    this.profileSub = this.profileService.getProfile(this.userId)
        .subscribe(
          profile => {
            this.isLoading = false;
            this.profile = profile;
            this.email = this.profile.email;
            this.fullName = this.profile.firstName + ' ' + this.profile.lastName;
             console.log(environment.authorisedAccounts);
            if (environment.authorisedAccounts.includes(this.email)) {
              this.isAllowed = true;
              console.log(this.isAllowed);
            }
          },
          error =>  {
            console.log(error);
            this.showErrorAlert('An error occurred', 'Information could not be fetched');
            this.navCtrl.setRoot(AuthPage);
          }
        )

  }

  openMenu() {
    this.menuCtrl.open();
  }

  closeMenu() {
    this.menuCtrl.close();
  }

  toggleMenu() {
    this.menuCtrl.toggle();
  }

   onNavigateToSavedItemsPage() {
    this.navCtrl.push(SavedItemsPage, {userId: this.userId});
  }

  onNavigateToPaystackPage() {
    this.navCtrl.push(PaystackPage, {
      userId: this.userId,
      email: this.email
      });
  }

  onNavigateToUploadDocumentPage() {
      return;
  }

  onNavigateToProfilePage() {
    this.navCtrl.push(ProfilePage, {userId: this.userId});
  }

  onNavigateToPaymentHistoryPage() {
    this.navCtrl.push(PaymentHistoryPage, {userId: this.userId,
      email: this.email});
  }

  onLogout()
  {
    this.loading = this.loadingCtrl
        .create({ spinner: 'crescent', content: 'Logging out...'});
    this.loading.present();
    this.authService.logout();
    this.showSuccessAlert('Logout Success', 'You have successfully logout', AuthPage);
    this.loading.dismiss();
  }


  private showSuccessAlert(title: string, message: string, linkpage: any) {
    let alert = this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'OK',
            handler: () => {
            this.navCtrl.setRoot(linkpage); 
        }
          },

        ]
      });
      alert.present();
  }

    async showErrorAlert(title: string, message: string) {
    let alert = await this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'Okay',
            role: 'cancel',
          },
        ]
      });
      await alert.present();
  }

  setProfileData() {
    return from(Plugins.Storage.get({key: 'authData'})).pipe(
            map(storedData => {
              console.log(storedData);
              const parsedData = JSON.parse(storedData.value) as  {
                    token: string; 
                    tokenExpirationDate: string; 
                    userId: string;
                    email: string;
              }
              console.log('This' + parsedData.email);   
            })
        );
  }
}