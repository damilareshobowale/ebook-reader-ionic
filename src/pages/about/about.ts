import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
declare var ePub: any;

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutModalPage {
  private book: any;
  public title: string;
  public creator: string;
  private publisher: string;
  private rights: string;
  private language: string;
  private identifier: string;
  private modified_date: string;

  constructor(public navCtrl: NavController, private navParams: NavParams, private view: ViewController) {

    this.book = ePub(navParams.data.book.file);
    this.book.loaded.metadata.then((meta) => {
      this.title = meta.title;
      this.creator = meta.creator;
      this.publisher = meta.publisher;
      this.rights = meta.rights;
      this.language = meta.language;
      this.identifier = meta.identifier;
      this.modified_date = meta.modified_date;

    });
  }

  closeModal() {
    this.view.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutModalPage');
  }
}
