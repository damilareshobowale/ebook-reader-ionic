import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { LoadingController, AlertController, NavController, MenuController, IonicPage, NavParams, Slides } from 'ionic-angular';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthService, AuthResponseData } from './auth.service';
import { ProfileService, ProfileResponseData } from '../profile/profile.service';
import { DashboardPage } from '../dashboard/dashboard';

/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
  providers: [AuthService, ProfileService],
})
export class AuthPage {
  isLoading = false;
  isLogin = true;
  title: string;
  loadingText: string;
  loading: any;
  alert: any;

  @ViewChild(Slides) slides: Slides;


  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private menuCtrl: MenuController,
  ) {
    this.menuCtrl.enable(false);
  }
  

  ngOnInit() {}

  authenticate(email?: string, password?: string, firstName?: string, lastName?: string) {
    this.isLoading = true;
    this.loadingText = 'Logging in...';
    if (!this.isLogin) {
      this.loadingText = 'Registration processing...';
    }
    // this.loading = this.loadingCtrl;
    this.loading = this.loadingCtrl
        .create({ spinner: 'crescent', content: this.loadingText});
    this.loading.present();
    let authObs: Observable<AuthResponseData>;
    let profileObs: Observable<ProfileResponseData[]>;
    if (this.isLogin) {
      authObs = this.authService.login(email, password);
      this.title = 'Login Success';
    } else {
      authObs = this.authService.signup(email, password);
      this.title = 'Registration Successful';
    }
    authObs.subscribe(
    resData => {
      if (!this.isLogin) {
          profileObs = this.profileService.addProfile(firstName, lastName, email, '',  '', resData.localId);
          profileObs.subscribe();
      }
      this.isLoading = false;
      this.loading.dismiss();
      if (!this.isLogin) {
         this.showSuccessAlert(this.title, 'Click [OK] to proceed to dashboard', resData.localId, email, password);
         return;
      }
      // this.showSuccessAlert(this.title, 'Click OK to go to dashboard', DashboardPage, resData.localId);
      this.navCtrl.setRoot(DashboardPage, {
        userId: resData.localId
      });
    },
    errRes => {
      console.log(errRes);
      this.loading.dismiss();
      if (errRes.statusText == "Unknown Error") {
        this.showErrorAlert('Network Failure', 'Please check your internet connection');
        return;
      }
      const code = errRes.error.error.message;
      // console.log()
      let message = 'Could not sign up, please try again';
      let title = 'Authentication Failed';
      if (code === 'EMAIL_EXISTS') {
        message = 'Your email addresss is taken!';
      } else if (code === 'EMAIL_NOT_FOUND') {
        message = 'Your email cannot be found';
      } else if (code === 'INVALID_PASSWORD') {
        message = 'Your email and password combination is not correct';
      }
      this.showErrorAlert(title, message);
    }
    );
  }

  onSwitchAuthMode() {
    this.isLogin = !this.isLogin;
  }

  onSubmit(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    const firstName = '';
    const lastName = '';
    this.isLogin = true;
    this.authenticate(email, password);
  }

  onSubmitReg(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    const confirmpassword = form.value.confirmpassword;
    const firstName = form.value.firstName;
    const lastName = form.value.lastName;
    if (password !== confirmpassword) {
      return this.showErrorAlert('Password Error', 'Password and confirm password is not equal');
    }
    this.isLogin = false;
    this.authenticate(email, password, firstName, lastName);
  }

  async showErrorAlert(title: string, message: string) {
    let alert = await this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'Okay',
            role: 'cancel',
          },
        ]
      });
      await alert.present();
  }

    async showSuccessAlert(title: string, message: string,  userId: string, email: string, password: string) {
    let alert = await this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'OK',
            handler: () => {
            this.isLogin =true;
            this.authenticate(email, password, '', ''); 
        }
          },

        ]
      });
      await alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthPage');
  }

  goToSlide() {
    this.slides.slideTo(2, 500);
  }

  goSignInSlides() {
    this.slides.slideTo(2);
  }

  goRegisterSlides() {
    this.slides.slideTo(3);
  }

  goNext() {
    this.slides.slideTo(1);
  }

}
