import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { NavController } from 'ionic-angular';
import { Observable, of } from 'rxjs';
import { take, tap, switchMap } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { DashboardPage } from '../dashboard/dashboard';

@Injectable()

export class AuthGuard implements CanLoad {
    user: any;

    constructor(private authService: AuthService, private router: Router, private navCtrl: NavController) {}

    canLoad(
        route: Route,
        segments: UrlSegment[]
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (!this.authService.userIsAutenticated)  {
            this.router.navigateByUrl('/');
        }
        return this.authService.userIsAutenticated.pipe(take(1),
        switchMap(isAuthenticated => {
            if (!isAuthenticated) {
                this.user = this.authService.autoLogin();
            } else {
                return of(isAuthenticated);
            }
        }),
        tap(isAuthenticated => {
            if (!isAuthenticated) {
                this.navCtrl.setRoot(DashboardPage, {
                    userId: this.user.userId,
                    email: this.user.email,
                })
            }
        }));
    }
}