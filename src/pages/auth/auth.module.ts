import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicPageModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { AuthPage } from './auth';
import { DashboardPage } from '../dashboard/dashboard'

const routes: Routes = [
  {
    path: '',
    component: AuthPage
  },
];

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicPageModule.forChild(AuthPage),
    RouterModule.forChild(routes),
    HttpClientModule,
  ],
})
export class AuthPageModule {}
