export class Profile {

    constructor(
        public id: string, 
        public firstName: string, 
        public lastName: string, 
        public email: string, 
        public userName: string, 
        public gender: string, 
        public userId: string,
    ) {}
}