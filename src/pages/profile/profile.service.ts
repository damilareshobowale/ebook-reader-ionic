import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { take, map, tap, delay, switchMap } from 'rxjs/operators';
import { Plugins } from '@capacitor/core';

import { Profile } from './profile.model';
import { environment } from '../../environments/environment';

export interface ProfileResponseData {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    userName: string;
    gender: string;
    userId: string;
}

@Injectable()

export class ProfileService {
    private _profiles = new BehaviorSubject<Profile[]>([]);

    constructor(private http: HttpClient) {}

    get profiles() {
        return this._profiles.asObservable();
    }

    fetchProfile(id: string) {
        return this.http.
            get<{ [key: string]: ProfileResponseData}>(
                `${environment.firebaseDatabaseURL}/profile/${id}.json`
            )
            .pipe(
                map(resData => {
                    const profiles = [];
                    for (const key in resData) {
                        if (resData.hasOwnProperty(key)) {
                            profiles.push(
                                new Profile(
                                    key,
                                    resData[key].firstName,
                                    resData[key].lastName,
                                    resData[key].email,
                                    resData[key].userName,
                                    resData[key].gender,
                                    resData[key].userId,
                                )
                            );
                        }
                    }
                    return profiles;
                }),
                tap(profiles => {
                    this._profiles.next(profiles);
                })
            )
    }

    getProfile(userId: string) {
        return this.http.
            get<ProfileResponseData>(
                `${environment.firebaseDatabaseURL}/profile/${userId}.json`
            ).pipe(map (resData => {
                    return new Profile (
                        userId,
                        resData.firstName,
                        resData.lastName,
                        resData.email,
                        resData.userName,
                        resData.gender,
                        resData.userId,
                    )
                })
            );
    }

    addProfile(
        firstName: string,
        lastName: string,
        email: string,
        userName: string,
        gender: string,
        userId: string,
    ) {
        let generateProfileId: string;
        const newProfile = new Profile(
            Math.random().toString(),
            firstName,
            lastName,
            email,
            userName,
            gender,
            userId,
        );
        return this.http.patch<{ name: string }>(
            `${environment.firebaseDatabaseURL}/profile/${userId}.json`, {
                ...newProfile,
                id: null,
            }
        )
        .pipe(
            switchMap(resData => {
                generateProfileId = resData.name;
                return this.profiles;
            }),
            take(1),
            tap(profiles => {
                newProfile.userId = generateProfileId;
                this._profiles.next(profiles);
            })
        );
    }

    updateProfile(userId: string, firstName: string, lastName: string, email: string, userName: string, phoneNumber: string, gender: string) {
        let updatedProfile: Profile[];
        return this.profiles.pipe(
            take(1),
            switchMap(profiles => {
            const updatedProfileIndex = profiles.findIndex(pr => pr.id === userId);
            updatedProfile = [...profiles];
            const oldProfile = updatedProfile[updatedProfileIndex];
            updatedProfile[updatedProfileIndex] = new Profile(
                oldProfile.id,
                oldProfile.firstName,
                oldProfile.lastName,
                oldProfile.email,
                oldProfile.userName,
                oldProfile.gender,
                oldProfile.userId,
            );
        return this.http.put(
                `${environment.firebaseDatabaseURL}/profile/${userId}.json`, {
                    ... updatedProfile[updatedProfileIndex], id: null
                })
        }), tap(() => {
            this._profiles.next(updatedProfile);
        })
        );
    }
}