import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoadingController, AlertController, NavController, IonicPage, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';


import { ProfileService, ProfileResponseData } from './profile.service';
import { Profile } from './profile.model';
import { AuthPage } from '../auth/auth';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [ProfileService],
})
export class ProfilePage implements OnInit {
  isLoading = false;
  loading: any;
  userId: string;
  profile: Profile;
  form: FormGroup;
  profileSub: Subscription;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private profileService: ProfileService,) {
      this.userId = navParams.get('userId');
      console.log(this.userId + ' this is ID')
  }


  ngOnInit() {
    this.isLoading = true;
    this.profileSub = this.profileService.getProfile(this.userId).subscribe(
          profile => {
            this.profile = profile;
            this.isLoading = false;
            this.form = new FormGroup({
              firstName: new FormControl(this.profile.firstName, {
                updateOn: 'blur',
                validators: [Validators.required]
              }),
              lastName: new FormControl (this.profile.lastName, {
                updateOn: 'blur',
              }),
              email: new FormControl (this.profile.email, {
                updateOn: 'blur',
                validators: [Validators.required]
              }),
              gender: new FormControl (this.profile.gender, {
                updateOn: 'blur',
              })
            })
          },
          error =>  {
            console.log(error);
            this.showErrorAlert('An error occurred', 'Information could not be fetched');
            this.navCtrl.setRoot(AuthPage);
          }
        )
  }

  updateProfile(firstName?: string, lastName?: string, email?: string, userName?: string, phoneNumber?: string, gender?: string) {
    this.isLoading = true;
    this.loading = this.loadingCtrl
        .create({ spinner: 'crescent', content: 'Updating profile...'});
    this.loading.present();
    this.profileService.updateProfile(this.userId, firstName, lastName, email, userName, phoneNumber, gender)
  }


  onSubmit(form: NgForm) {
    const firstName = form.value.firstName;
    const lastName = form.value.lastName;
    const email = form.value.email;
    const userName = form.value.userName;
    const phoneNumber = form.value.phoneNumber;
    const gender = form.value.gender;

    this.updateProfile(firstName, lastName, email, userName, phoneNumber, gender);
  }

  private showErrorAlert(title: string, message: string) {
    let alert = this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'Okay',
            role: 'cancel',
          },
        ]
      });
      alert.present();
  }

    private showSuccessAlert(title: string, message: string, linkpage: any) {
    let alert = this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'OK',
            handler: () => {
            this.navCtrl.setRoot(linkpage); 
        }
          },

        ]
      });
      alert.present();
  }
}
