import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { Bookmarks } from '../bookmarks/bookmarks';
import { Highlights } from '../highlights/highlights';
import { TableOfContentsPage } from '../table-of-contents/table-of-contents';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-book-details-modal',
  templateUrl: 'book-details-modal.html',
})
export class BookDetailsModal {
  private toc: any;
  private bookmarks: any;
  private highlights: any;
  private curentLocations: any;
  private modbookid: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private events: Events, private storage: Storage) {

    this.toc = navParams.data.toc;
    this.bookmarks = navParams.data.bookmarks;
    this.highlights = navParams.data.highlights;

    this.events.subscribe("booksDetailsModalShow", (content) => {
      if (!content) {
        this.closeModalBook();
      }
    });
  }

  closeModalBook() {
    this.view.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModuleBookPage');
  }

  tab1root = TableOfContentsPage;
  tab2root = Bookmarks;
  tab3root = Highlights;

}

