import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookDetailsModal } from './book-details-modal';

@NgModule({
  declarations: [
    BookDetailsModal,
  ],
  imports: [
    IonicPageModule.forChild(BookDetailsModal),
  ],
})
export class BookDetailsModalPageModule { }
