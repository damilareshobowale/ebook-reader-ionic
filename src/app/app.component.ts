import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Nav, Platform, MenuController, LoadingController, AlertController, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Subscription, from, of } from 'rxjs';
import { map, take, tap, switchMap } from 'rxjs/operators';
import { Plugins } from '@capacitor/core';
import { Storage } from '@ionic/storage';

import { DbService } from '../providers/db-service';
import { CONFIG } from '../providers/app-config';
import { BooksPage } from '../pages/books/books';
import { ProfilePage } from '../pages/profile/profile';
import { SavedItemsPage } from '../pages/saved-items/saved-items';
import { AuthPage } from '../pages/auth/auth';
import { AuthService } from '../pages/auth/auth.service';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { PaystackPage } from '../pages/paystack/paystack';
import { SubscriptionPage } from '../pages/subscription/subscription';
import { PaymentHistoryPage } from '../pages/payment-history/payment-history';
import { User } from '../pages/auth/user.model';

@Component({
  templateUrl: 'app.html',
  providers: [AuthService]
})

export class MyApp implements OnInit, OnDestroy {
  private authSub: Subscription;
  private previousAuthState = false;
  userId: string;
  user: any;
  authData: any;
  @ViewChild(Nav) nav: Nav;
  loading: any;
  rootPage: any = AuthPage;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public dbService: DbService,
    public authService: AuthService,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public storage: Storage) {

    this.dbService = dbService;
    this.initializeApp();
    platform.ready().then(() => {
      this.hideSplashScreen();
    });
    const expirationTime = new Date(new Date().getTime() + (3600 * 1000));
    Plugins.Storage.get({key: 'authData'}).then((value: any) => {
      if (value != null) {
        this.nav.setRoot(DashboardPage, {
          userId: value.userId,
          email: value.email,
          token: value.token,
          tokenExpirationDate: expirationTime.toISOString(),
        })
      }
      const user = new User(value.userId, value.email, value.token, expirationTime);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.hideSplashScreen();
      this.dbService.init();
    });
  }

  ngOnInit() {
    // this.authSub = this.authService.autoLogin().subscribe(isAuth => {
    //   if (!isAuth && this.previousAuthState != isAuth) {
        
    //     console.log('this is ' +isAuth);
    //     this.nav.setRoot(AuthPage);
    //   }
    //   this.previousAuthState = isAuth;
    //   this.nav.setRoot(DashboardPage, {
    //     userId: isAuth.userId,
    //     email: isAuth.email,
    //   });
    // });

    // this.user = this.authService.autoLogin().subscribe(isAuth => {
    //     console.log(isAuth);
    // });
    // console.log(this.user);
;
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(false, "header-menu");
  }
  
  hideSplashScreen() {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    }
  }

  onLogout()
  {
    this.loading = this.loadingCtrl
        .create({ spinner: 'crescent', content: 'Logging out...'});
    this.loading.present();
    this.authService.logout();
    this.menuCtrl.close();
    this.showSuccessAlert('Logout Success', 'You have successfully logout', AuthPage);
    this.loading.dismiss();
  }

  
  private showSuccessAlert(title: string, message: string, linkpage: any) {
    let alert = this.alertCtrl
      .create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'OK',
            handler: () => {
            this.nav.setRoot(linkpage); 
        }
          },

        ]
      });
      alert.present();
  }

  ngOnDestroy() {
    if (this.authSub) {
      this.authSub.unsubscribe();
    }
  }

  onNavigateToHome() {
    this.nav.push(DashboardPage);
    this.menuCtrl.close();
  }

  onNavigateToSavedItemsPage() {
    this.nav.push(SavedItemsPage);
    this.menuCtrl.close();
  }

  onNavigateToPaystackPage() {
    this.nav.push(PaystackPage);
    this.menuCtrl.close();
  }

  onNavigateToProfilePage() {
    this.nav.push(ProfilePage);
    this.menuCtrl.close();
  }

  onNavigateToPaymentHistoryPage() {
    this.nav.push(PaymentHistoryPage);
    this.menuCtrl.close();
  }

  onNavigateToBooksPage() {
    this.nav.push(BooksPage);
    this.menuCtrl.close();
  }

  onNavigateToSubscriptionPage() {
    this.nav.push(SubscriptionPage);
    this.menuCtrl.close();
  }
}
