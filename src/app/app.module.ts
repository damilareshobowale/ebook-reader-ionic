import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { Angular4PaystackModule } from 'angular4-paystack';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { DocumentViewer } from '@ionic-native/document-viewer';

import { MyApp } from './app.component';
import { HeaderMenuComponent } from '../components/header-menu/header-menu';
import { BookPage } from '../pages/book/book';
import { Highlights } from '../pages/highlights/highlights';
import { Bookmarks } from '../pages/bookmarks/bookmarks';
import { BooksPage } from '../pages/books/books'
import { AuthPage } from '../pages/auth/auth';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { PaystackPage } from '../pages/paystack/paystack';
import { ProfilePage } from '../pages/profile/profile';
import { SubscriptionPage } from '../pages/subscription/subscription';
import { SavedItemsPage } from '../pages/saved-items/saved-items';
import { TableOfContentsPage } from '../pages/table-of-contents/table-of-contents';
import { SettingsPage } from '../pages/settings/settings';
import { PaymentHistoryPage } from '../pages/payment-history/payment-history';
import { DbService } from '../providers/db-service';
import { ParseBook } from '../providers/parse-db-book';
import { SQLite } from '@ionic-native/sqlite';
import { AppRoutingModule } from './app-routing.module';


    // MyApp,
    // BookPage,
    // Highlights,
    // Bookmarks,
    // BooksPage,
    // AuthPage,
    // DashboardPage,
    // TableOfContentsPage,
    // SettingsPage,
    // PaystackPage,
    // SubscriptionPage,
    // PaymentHistoryPage,

@NgModule({
  declarations: [
    MyApp,
    BookPage,
    Highlights,
    Bookmarks,
    BooksPage,
    AuthPage,
    DashboardPage,
    TableOfContentsPage,
    SettingsPage,
    PaystackPage,
    SubscriptionPage,
    PaymentHistoryPage,
    ProfilePage,
    SavedItemsPage,
    HeaderMenuComponent,
    ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'top',
      platforms: {
        android: {
          tabsPlacement: 'top'
        },
        ios: {
          tabsPlacement: 'top'
        },
        windows:
          {
            tabsPlacement: 'top'
          }
      },
      locationStrategy: 'path',
    }),
    HttpClientModule,
    Angular4PaystackModule,
    IonicStorageModule.forRoot(),
    AppRoutingModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BookPage,
    Highlights,
    Bookmarks,
    BooksPage,
    AuthPage,
    DashboardPage,
    TableOfContentsPage,
    SettingsPage,
    PaystackPage,
    SubscriptionPage,
    PaymentHistoryPage,
    ProfilePage,
    SavedItemsPage,
    HeaderMenuComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DbService,
    ParseBook,
    SQLite,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    File,
    FileTransfer,
    DocumentViewer,
  ]
})
export class AppModule {}
